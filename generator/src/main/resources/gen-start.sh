spark2-submit \
--class "org.me.stringsorter.generator.Main" \
--master yarn \
--deploy-mode client \
--executor-memory 1G \
--driver-memory 1G \
--num-executors 4 \
--executor-cores 1 \
--principal principal \
--keytab /home/principal/principal.keytab \
generator-1.0-SNAPSHOT.jar \
10000000000 \
100 \
/tmp/useinov-kv/test