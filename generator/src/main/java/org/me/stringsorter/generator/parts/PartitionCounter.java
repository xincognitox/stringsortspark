package org.me.stringsorter.generator.parts;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PartitionCounter {

    public PartitionCounter() {
        //default
    }

    public int countPartitions(long numRows, int maxLength, int symbolSize) {
        BigDecimal maxSymbols = new BigDecimal(numRows);
        maxSymbols = maxSymbols.multiply(BigDecimal.valueOf(maxLength));

        if (maxSymbols.intValue() == 0 ){
            return 1;
        }

        BigDecimal lineSeparators = new BigDecimal((numRows - 1) * 2);
        maxSymbols = maxSymbols.add(lineSeparators);

        BigDecimal avgSymbols = maxSymbols.divide(BigDecimal.valueOf(2), RoundingMode.UP);

        BigDecimal totalSize = new BigDecimal(symbolSize).multiply(avgSymbols);
        BigDecimal totalSizeMb = totalSize.divide(new BigDecimal(128 * 1024 * 1024), RoundingMode.UP);

        return totalSizeMb.intValue();
    }
}
