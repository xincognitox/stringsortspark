package org.me.stringsorter.generator.data;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.spark.mllib.random.RandomDataGenerator;

import java.util.concurrent.ThreadLocalRandom;

public class RandomDataGeneratorImpl implements RandomDataGenerator<String> {

    private int maxLength;

    public RandomDataGeneratorImpl(int maxLength) {
        this.maxLength = maxLength;
    }

    public String nextValue() {
        int length = ThreadLocalRandom.current().nextInt(maxLength);
        return RandomStringUtils.random(length, true, true);
    }

    public RandomDataGenerator<String> copy() {
        return new RandomDataGeneratorImpl(maxLength);
    }

    public void setSeed(long l) {
        //not needed
    }
}
