package org.me.stringsorter.generator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.random.RandomRDDs;
import org.me.stringsorter.generator.data.RandomDataGeneratorImpl;
import org.me.stringsorter.generator.parts.PartitionCounter;

public class Main {

    /**
     * @param args [0] - number of strings
     *             [1] - max string length
     *             [2] - output path
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            throw new IllegalArgumentException("Wrong argument count. " +
                    "Please enter correct arguments: [0] - number of strings, [1] - max string length, [2] - output path");
        }

        long numRows = Long.parseLong(args[0]);
        int maxLength = Integer.parseInt(args[1]);
        String outputPath = args[2];

        SparkConf sparkConf = new SparkConf().setAppName("StringSorter-Generator");
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        PartitionCounter partitionCounter = new PartitionCounter();
        int numPartitions = partitionCounter.countPartitions(numRows, maxLength, 1);

        RandomDataGeneratorImpl dataGenerator = new RandomDataGeneratorImpl(maxLength);
        JavaRDD<String> stringJavaRDD = RandomRDDs.randomJavaRDD(sparkContext, dataGenerator, numRows, numPartitions);

        stringJavaRDD.saveAsTextFile(outputPath);
    }
}
