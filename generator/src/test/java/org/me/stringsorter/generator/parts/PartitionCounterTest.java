package org.me.stringsorter.generator.parts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(value = Parameterized.class)
public class PartitionCounterTest {

    @Parameterized.Parameter
    public long numRows;

    @Parameterized.Parameter(1)
    public int maxLength;

    @Parameterized.Parameter(2)
    public int expected;

    @Parameterized.Parameters
    public static Collection<Object[]> getTestCases() {
        return Arrays.asList(new Object[][]{
                {1, 1, 1},
                {0, 0, 1},
                {1048576, 1280, 6},
        });
    }

    @Test
    public void countPartitions() {
        PartitionCounter partitionCounter = new PartitionCounter();
        int result = partitionCounter.countPartitions(numRows, maxLength, 1);
        assertEquals(expected, result);
    }
}