spark2-submit \
--class "org.me.stringsorter.sorter.Main" \
--master yarn \
--deploy-mode client \
--executor-memory 1G \
--driver-memory 1G \
--num-executors 4 \
--executor-cores 1 \
--principal principal \
--keytab /home/principal/principal.keytab \
sorter-1.0-SNAPSHOT.jar \
/tmp/useinov-kv/test \
/tmp/useinov-kv/sorted \
range