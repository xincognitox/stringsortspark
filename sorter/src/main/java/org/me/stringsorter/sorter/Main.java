package org.me.stringsorter.sorter;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.me.stringsorter.sorter.sort.StringSorter;
import org.me.stringsorter.sorter.sort.StringSorterRangePartitioner;
import org.me.stringsorter.sorter.sort.custom.StringSorterCustom;
import org.me.stringsorter.sorter.utils.HdfsSizeEstimator;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            throw new IllegalArgumentException("Wrong argument count. " +
                    "Please enter correct arguments: [0] - input path, [1] - output path, [2] - mode (custom or range)");
        }

        String inputPath = args[0];
        String outputPath = args[1];
        String mode = args[2];

        SparkConf sparkConf = new SparkConf().setAppName("StringSorter-Sorter");
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        HdfsSizeEstimator hdfsSizeEstimator = new HdfsSizeEstimator(sparkContext);
        long inputSize = hdfsSizeEstimator.estimateSize(inputPath);

        JavaRDD<String> stringJavaRDD = sparkContext.textFile(inputPath);

        StringSorter stringSorter;
        switch (mode) {
            case "custom":
                stringSorter = new StringSorterCustom();
                break;
            case "range":
                stringSorter = new StringSorterRangePartitioner();
                break;
            default:
                throw new IllegalArgumentException("Wrong mode. Correct values: custom or range");
        }

        JavaRDD<String> sorted = stringSorter.sortStringJavaRDD(stringJavaRDD, inputSize);
        sorted.saveAsTextFile(outputPath);

        sparkContext.close();
    }
}
