package org.me.stringsorter.sorter.sort;

import org.apache.spark.api.java.JavaRDD;

import java.io.Serializable;

public interface StringSorter extends Serializable {
    JavaRDD<String> sortStringJavaRDD(JavaRDD<String> stringJavaRDD, long inputSize);
}
