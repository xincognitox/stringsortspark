package org.me.stringsorter.sorter.sort.custom;

import java.io.Serializable;

class StringRanker implements Serializable {

    int rankString(String s) {
        if (s.isEmpty()) {
            return 0;
        }
        return s.getBytes()[0];
    }
}
