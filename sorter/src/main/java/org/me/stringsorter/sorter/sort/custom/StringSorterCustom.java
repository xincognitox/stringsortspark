package org.me.stringsorter.sorter.sort.custom;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.me.stringsorter.sorter.sort.StringSorter;
import org.me.stringsorter.sorter.utils.PartitionsCounter;
import org.spark_project.guava.collect.Lists;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class StringSorterCustom implements StringSorter {

    private StringRanker stringRanker = new StringRanker();

    public StringSorterCustom() {
        //default
    }

    @Override
    public JavaRDD<String> sortStringJavaRDD(JavaRDD<String> stringJavaRDD, long inputSize) {
        JavaPairRDD<String, Integer> rankedStrings = stringJavaRDD.mapToPair(s -> new Tuple2<>(s, stringRanker.rankString(s)));

        List<Integer> ranks = rankedStrings.values().distinct().collect();
        int numPartitions = new PartitionsCounter().countPartitions(inputSize);
        List<List<Integer>> partitions = Lists.partition(ranks, numPartitions);
        List<List<Integer>> serializablePartitions = new ArrayList<>();
        for (List<Integer> partition : partitions) {
            serializablePartitions.add(new ArrayList<>(partition));
        }

        StringPartitioner stringPartitioner = new StringPartitioner(stringRanker, serializablePartitions);
        JavaPairRDD<String, Integer> sorted = rankedStrings.repartitionAndSortWithinPartitions(stringPartitioner);
        return sorted.keys();
    }
}
