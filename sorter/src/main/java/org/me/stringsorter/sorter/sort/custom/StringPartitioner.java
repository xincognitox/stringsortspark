package org.me.stringsorter.sorter.sort.custom;

import org.apache.spark.Partitioner;

import java.util.List;

public class StringPartitioner extends Partitioner {

    private StringRanker stringRanker;
    private List<List<Integer>> partitions;

    StringPartitioner(StringRanker stringRanker, List<List<Integer>> partitions) {
        this.stringRanker = stringRanker;
        this.partitions = partitions;
    }

    @Override
    public int numPartitions() {
        return partitions.size();
    }

    @Override
    public int getPartition(Object o) {
        String s = (String) o;
        int rank = stringRanker.rankString(s);

        for (int i = 0; i < partitions.size(); i++) {
            List<Integer> partition = partitions.get(i);
            if (partition.contains(rank)) {
                return i;
            }
        }

        return 0;
    }
}
