package org.me.stringsorter.sorter.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PartitionsCounter {
    public int countPartitions(long inputSize){
        BigDecimal blockSize = new BigDecimal(128 * 1024 * 1024);
        BigDecimal result = new BigDecimal(inputSize).divide(blockSize, RoundingMode.UP);
        return result.intValue();
    }
}
