package org.me.stringsorter.sorter.sort;

import org.apache.spark.RangePartitioner;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.me.stringsorter.sorter.utils.PartitionsCounter;
import scala.Tuple2;
import scala.math.Ordering;
import scala.math.Ordering$;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;

import java.util.Comparator;

public class StringSorterRangePartitioner implements StringSorter {

    public StringSorterRangePartitioner() {
        //default
    }

    @Override
    public JavaRDD<String> sortStringJavaRDD(JavaRDD<String> stringJavaRDD, long inputSize) {
        JavaPairRDD<String, Integer> pairRDD = stringJavaRDD.mapToPair(s -> new Tuple2<>(s, 1));

        int numPartitions = new PartitionsCounter().countPartitions(inputSize);

        Comparator<String> comparator = Comparator.naturalOrder();
        Ordering<String> ordering = Ordering$.MODULE$.comparatorToOrdering(comparator);
        ClassTag<String> classTag = ClassTag$.MODULE$.apply(String.class);
        RangePartitioner<String, Integer> rangePartitioner = new RangePartitioner<>(numPartitions, pairRDD.rdd(), true, ordering, classTag);

        return pairRDD.repartitionAndSortWithinPartitions(rangePartitioner)
                .keys();
    }
}
