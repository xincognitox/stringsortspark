package org.me.stringsorter.sorter.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

public class HdfsSizeEstimator {

    private JavaSparkContext sparkContext;

    public HdfsSizeEstimator(JavaSparkContext sparkContext) {
        this.sparkContext = sparkContext;
    }

    public long estimateSize(String inputPath) throws IOException {
        Configuration configuration = sparkContext.hadoopConfiguration();
        FileSystem fileSystem = FileSystem.get(configuration);
        Path path = new Path(inputPath);
        return fileSystem.getContentSummary(path).getLength();
    }

}
